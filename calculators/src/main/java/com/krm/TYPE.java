package com.krm;

public enum TYPE {
	emplsOrderInWeekDays,
	emplsOrderInWeekEnds,
	startEmplInWeekDays,
	startEmplInWeekEnds,
	initialDate
}
