package com.krm;

import com.store.Employee;

import java.time.LocalDate;
import java.util.HashMap;

public interface Calculator {
	HashMap<LocalDate, Employee> generate(LocalDate from, long durationsDayFromInitDate);
	LocalDate getInitialDate();
}
