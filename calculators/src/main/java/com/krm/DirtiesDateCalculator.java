package com.krm;

import com.store.Employee;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.LongStream;

import static com.store.Employee.USER2;


public class DirtiesDateCalculator implements Calculator {
	private static final Logger LOG = LogManager.getLogger(DirtiesDateCalculator.class);

	private final LocalDate initialDate = LocalDate.of(2017, 12, 1);

	HashMap<LocalDate, Employee> store = new HashMap<>();

	@Override
	public HashMap<LocalDate, Employee> generate(LocalDate from, long durationsDay) {
		long initDurations = ChronoUnit.DAYS.between(initialDate, from);
		AtomicInteger aWDC = new AtomicInteger(USER2.toID());

		Function<AtomicInteger, Employee> func = i -> Employee.get(i.get());

		LongStream
				.rangeClosed(0, durationsDay  + initDurations)
				.mapToObj(this.initialDate::plusDays)
				.forEachOrdered(d -> {
					switch (d.getDayOfWeek().getValue()) {
						case 2: {
							store.put(d, func.apply(aWDC));
							store.put(d.plusDays(4), func.apply(aWDC));
							store.put(d.plusDays(5), func.apply(aWDC));
							aWDC.incrementAndGet();
							if (aWDC.get() > 7) aWDC.set(1);
							break;
						}
						case 6:
							break;
						case 7:
							break;
						default: {
							store.put(d, func.apply(aWDC));
							aWDC.incrementAndGet();
							if (aWDC.get() > 7) aWDC.set(1);
						}
					}
				});
		return store;
	}

	public LocalDate getInitialDate() {
		return initialDate;
	}
}
