package com.krm;

import com.store.Employee;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.stream.LongStream;

import static com.store.Employee.*;
import static java.time.DayOfWeek.*;

public class DirtiesDateCalculator_5 implements Calculator {
	private static final Logger LOG = LogManager.getLogger(DirtiesDateCalculator.class);

	private final HashMap<LocalDate, Employee> store = new HashMap<>();

	private Employee[] employees = {
			EMPTY, USER1, USER2,
			USER3, USER5, USER6
	};

	private Employee[] employeesWE = {
			EMPTY, USER1, USER2,
			USER6, USER5, USER4
	};

	private final AtomicInteger aWDC = new AtomicInteger(USER3.toID());
	private final AtomicInteger aWEC = new AtomicInteger(USER1.toID());

	private final LocalDate initialDate = LocalDate.of(2018, 4, 11);

	private final Consumer<AtomicInteger> cInc = ai -> {
		if (ai.incrementAndGet() > employees.length - 1) ai.set(1);
	};

	private final Consumer<LocalDate> cDate = d -> {
		switch (d.getDayOfWeek()) {
			case MONDAY:
				store.put(d, employees[aWDC.get()]);
				store.put(d.with(TemporalAdjusters.next(TUESDAY)), employees[aWDC.get()]);
				cInc.accept(aWDC);
				break;
			case TUESDAY:
				store.put(d, employees[aWDC.get()]);
				store.put(d.with(TemporalAdjusters.next(WEDNESDAY)), employees[aWDC.get()]);
				cInc.accept(aWDC);
				break;
			case WEDNESDAY:
				store.put(d, employees[aWDC.get()]);
				store.put(d.with(TemporalAdjusters.next(THURSDAY)), employees[aWDC.get()]);
				cInc.accept(aWDC);
				break;
			case THURSDAY:
				store.put(d, employees[aWDC.get()]);
				store.put(d.with(TemporalAdjusters.next(FRIDAY)), employees[aWDC.get()]);
				cInc.accept(aWDC);
				break;
			case FRIDAY:
				store.put(d, employees[aWDC.get()]);
				store.put(d.with(TemporalAdjusters.next(MONDAY)), employees[aWDC.get()]);
				break;
			case SATURDAY: {
				store.put(d, employeesWE[aWEC.get()]);
				store.put(d.plusDays(1), employeesWE[aWEC.get()]);
				cInc.accept(aWEC);
				break;
			}
			case SUNDAY:
				break;
		}
	};

	@Override
	public HashMap<LocalDate, Employee> generate(LocalDate from, long durationsDay) {
		long initDurations = ChronoUnit.DAYS.between(initialDate, from);
		LongStream
				.rangeClosed(0, initDurations + durationsDay)
				.mapToObj(this.initialDate::plusDays)
				.forEachOrdered(cDate);
		return store;
	}

	public LocalDate getInitialDate() {
		return initialDate;
	}
}
