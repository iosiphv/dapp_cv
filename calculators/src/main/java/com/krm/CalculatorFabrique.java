package com.krm;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javafx.util.Pair;


public class CalculatorFabrique {

	private List<Pair<LocalDate, Calculator>> list;

	private class Entity {
		private LocalDate from;
		private LocalDate to;
		private Calculator calculator;
	}

	public CalculatorFabrique() {
		list = Stream
				.of(new DirtiesDateCalculator(),
				    new DirtiesDateCalculator_5(),
				    new DirtiesDateCalculator_6())
				.map(c -> new Pair<LocalDate, Calculator>(c.getInitialDate(), c))
				.sorted(Comparator.comparing(Pair::getKey))
				.collect(Collectors.toList());
	}


	public static void main(String[] args) {
		CalculatorFabrique cf = new CalculatorFabrique();
		cf.printArray();

		cf.getCalc(LocalDate.now()).forEach(c -> System.out.println(c.getClass()));
	}

	private List<Calculator> getCalc(LocalDate date) {
		list.stream()
		    .map(p -> p.getKey().compareTo(date));
		return null;
	}

	private void printArray() {
		list.forEach(p -> System.out.println(p.getKey() + " " + p.getValue().getClass()));
	}
}
