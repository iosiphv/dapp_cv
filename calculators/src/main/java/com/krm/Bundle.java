package com.krm;

import com.store.Employee;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;

import static com.store.Employee.*;

public class Bundle {
	private final HashMap<TYPE, Object> bundle = new HashMap<>(TYPE.values().length);

	public Bundle() {
		bundle.put(TYPE.emplsOrderInWeekDays, new Employee[]{
				EMPTY, USER1, USER2,
				USER3, USER4, USER5
		});

		bundle.put(TYPE.emplsOrderInWeekEnds, new Employee[]{
				EMPTY, USER1, USER2,
				USER5, USER4, USER3
		});

		bundle.put(TYPE.startEmplInWeekDays, new AtomicInteger(USER3.toID()));

		bundle.put(TYPE.startEmplInWeekEnds, new AtomicInteger(USER1.toID()));

		bundle.put(TYPE.initialDate, LocalDate.of(2018, 4, 11));
	}

	public <P> P getParameter(TYPE type, Class<P> c) {
		return (P) bundle.get(type);
	}
}
