package com.krm;

import com.store.Employee;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.LongStream;

import static com.store.Employee.*;
import static com.store.Employee.USER4;

public class DirtiesDateCalculator_6 implements Calculator {
	private static final Logger LOG = LogManager.getLogger(DirtiesDateCalculator.class);

	private Employee[] employees = {
			EMPTY,
			USER1, USER2, USER3,
			USER4, USER5, USER6
	};

	private Employee[] employeesWE = {
			EMPTY,
			USER4, USER1, USER2,
			USER6, USER5, USER4
	};

	private final LocalDate initialDate = LocalDate.of(2018, 2, 16);

	private final HashMap<LocalDate, Employee> store = new HashMap<>();

	@Override
	public HashMap<LocalDate, Employee> generate(LocalDate from, long durationsDay) {
		long initDurations = ChronoUnit.DAYS.between(initialDate, from);
		AtomicInteger aWDC = new AtomicInteger(USER1.toID());
		AtomicInteger aWEC = new AtomicInteger(USER5.toID());

		LongStream
				.rangeClosed(0, durationsDay + initDurations)
				.mapToObj(this.initialDate::plusDays)
				.forEachOrdered(d -> {
					                switch (d.getDayOfWeek().getValue()) {
						                case 6:
							                store.put(d, employeesWE[aWEC.get()]);
							                store.put(d.plusDays(1), employeesWE[aWEC.get()]);
							                if (aWEC.incrementAndGet() > employeesWE.length - 1) aWEC.set(1);
							                break;
						                case 7:
							                break;
						                default:
							                store.put(d, employees[aWDC.get()]);
							                if (aWDC.incrementAndGet() > employees.length - 1) aWDC.set(1);
							                break;

					                }
				                }
				);
		return store;
	}


	public LocalDate getInitialDate() {
		return initialDate;
	}
}
