package com.krm;


import com.store.Employee;
import com.store.db.DBConnect;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjuster;
import java.time.temporal.TemporalAdjusters;
import java.time.temporal.TemporalField;
import java.util.HashMap;

public class CalculatorUtils {

	private static final Logger LOG = LogManager.getLogger(CalculatorUtils.class);

	public static long toDateExclude(LocalDate from, int toYear, Month toMonth) {
		LocalDate firstDayOfMonth = LocalDate.of(toYear, toMonth, 1);

		LocalDate startDate = firstDayOfMonth
				.minusMonths(3)
				.with(TemporalAdjusters.dayOfWeekInMonth(0, DayOfWeek.MONDAY));

		LocalDate endDate = startDate
				.plusMonths(3)
				.with(TemporalAdjusters.lastDayOfMonth())
				.plusDays(1)
				.with(TemporalAdjusters.firstInMonth(DayOfWeek.SUNDAY));
		LOG.debug("CalculatorUtils {} to {}", startDate, endDate);
		return ChronoUnit.DAYS.between(from, endDate);
	}

	public static void main(String[] args) {
		DBConnect db = DBConnect.getInstance();

		db.truncDB();

		LocalDate initialDate = LocalDate.now().plusMonths(1);
		LocalDate endDate     = initialDate.plusMonths(3);

		long dur = CalculatorUtils.toDateExclude(initialDate, endDate.getYear(), endDate.getMonth());

		Calculator calc = new DirtiesDateCalculator_5();

		HashMap<LocalDate, Employee> map = calc.generate(initialDate, dur);

		map.forEach((date, employee) -> {
			LOG.debug("insert impl:{} date:{}", employee, date);
			db.insert(employee, date);
		});
	}
}
