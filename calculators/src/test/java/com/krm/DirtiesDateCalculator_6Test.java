package com.krm;

import com.store.Employee;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;

import static com.store.Employee.*;
import static java.time.Month.FEBRUARY;
import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class DirtiesDateCalculator_6Test {
	DirtiesDateCalculator_6 calc = null;
	private HashMap<LocalDate, Employee> emplD;
	Employee expected;
	LocalDate date;

	public DirtiesDateCalculator_6Test(Employee empl, LocalDate date) {
		this.expected = empl;
		this.date = date;
	}

	@Parameterized.Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][]{
				{USER1, LocalDate.of(2018, FEBRUARY, 16)},
				{USER5, LocalDate.of(2018, FEBRUARY, 17)},
				{USER5, LocalDate.of(2018, FEBRUARY, 18)},
				{USER2, LocalDate.of(2018, FEBRUARY, 19)},
				{USER3, LocalDate.of(2018, FEBRUARY, 20)},
				{USER4, LocalDate.of(2018, FEBRUARY, 21)},
				{USER5, LocalDate.of(2018, FEBRUARY, 22)},
				{USER6, LocalDate.of(2018, FEBRUARY, 23)},
				{USER1, LocalDate.of(2018, FEBRUARY, 26)},
				{USER2, LocalDate.of(2018, FEBRUARY, 27)}
		});
	}

	@Before
	public void setUp() {
		calc = new DirtiesDateCalculator_6();
		long duration = CalculatorUtils.toDateExclude(calc.getInitialDate(), 2018, Month.APRIL);
		emplD = calc.generate(initialDate, duration);
	}

	@Test
	public void test() {
		Employee actual = emplD.get(date);
		System.out.println("date:" + date + " exp:" + expected + " act:" + actual);
		assertEquals(expected, actual);
	}
}