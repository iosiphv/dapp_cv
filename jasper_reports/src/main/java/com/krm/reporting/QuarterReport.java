package com.krm.reporting;

import com.krm.Calculator;
import com.krm.DirtiesDateCalculator;
import com.store.ContentResolver;
import com.store.Employee;
import com.store.db.DBConnect;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.ListOfArrayDataSource;
import net.sf.jasperreports.view.JasperViewer;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDate;
import java.time.Month;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjuster;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.function.Function;
import java.util.stream.LongStream;

public class QuarterReport {
	private Logger LOG;

	private String   QUARTER_REPORT_JRXML;
	private String   MASTER_REPORT_JRXML;
	private String[] WEEK_DAYS_NAME;

	private final ContentResolver<Employee> content = DBConnect.getInstance();
	private       JasperReport              subJasperReport;
	private       JasperPrint               print;
	private       LocalDate                 startDate;

	public QuarterReport(LocalDate toDate) {
		String path = QuarterReport.class.getResource("/jrxml_reports/quarter_report/").getPath();
		QUARTER_REPORT_JRXML = path + "quarter_report.jrxml";
		MASTER_REPORT_JRXML = path + "masterReport.jrxml";

		WEEK_DAYS_NAME = new String[]{
				"monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"
		};

		LOG = LogManager.getLogger(QuarterReport.class);

		startDate = toDate.minusMonths(3);
		LOG.debug("from {} to {}", startDate, toDate);
		LOG.debug("max date {}", content.getMaxDate());


		if (content.getMaxDate().isBefore(toDate)) {
//			Calculator localCalc = new DirtiesDateCalculator();
//			localCalc.toDate(toDate.getYear(), toDate.getMonth());
		}
	}

	public void build() {
		LOG.info("compilling...\n\t\t{}\n\t\t{}", MASTER_REPORT_JRXML, QUARTER_REPORT_JRXML);
		try {
			subJasperReport = JasperCompileManager.compileReport(QUARTER_REPORT_JRXML);
			JasperReport masterJasperReport = JasperCompileManager.compileReport(MASTER_REPORT_JRXML);
			print = JasperFillManager.fillReport(masterJasperReport,
					fillParameters(),
					new JREmptyDataSource(1));
		} catch (JRException e) {
			LOG.error(e.getLocalizedMessage());
			LOG.throwing(Level.FATAL, e);
		}
	}

	private HashMap<String, Object> fillParameters() {
		LOG.info("filling params...");
		HashMap<String, Object> parameters = new HashMap<>(5);

		parameters.put("subReport", subJasperReport);
		parameters.put("subDataSource1", fillingSubReportDataSource(startDate));
		parameters.put("subDataSource2", fillingSubReportDataSource(startDate.plusMonths(1)));
		parameters.put("subDataSource3", fillingSubReportDataSource(startDate.plusMonths(2)));

		LinkedList<String> list = new LinkedList<>();

		list.add(cyr_MonthName_TxtRepr(startDate.getMonth()) + " " + startDate.getYear());
		list.add(cyr_MonthName_TxtRepr(startDate.plusMonths(1).getMonth()) + " " + startDate.plusMonths(
				1).getYear());
		list.add(cyr_MonthName_TxtRepr(startDate.plusMonths(2).getMonth()) + " " + startDate.plusMonths(
				2).getYear());

		parameters.put("monthList", list);
		LOG.info("MONTH list {}", list);
		return parameters;
	}

	private JRDataSource fillingSubReportDataSource(LocalDate date) {
		LocalDate lastMonthDate = date.with(TemporalAdjusters.lastDayOfMonth());

		Function<LocalDate, Integer> dayNumFunc = d -> d.getDayOfWeek().getValue();
		Function<LocalDate, Boolean> func       = d -> lastMonthDate.isAfter(d) || lastMonthDate.isEqual(d);

		LocalDate endDate = date
				.with(TemporalAdjusters.firstDayOfNextMonth())
				.with(TemporalAdjusters.dayOfWeekInMonth(1, DayOfWeek.SUNDAY));

		long count = ChronoUnit.DAYS.between(date, endDate);


		LOG.info("local range {} from {} to {} days {}", date.getMonth(), date, endDate, count);

		ArrayList<Object[]> list = new ArrayList<>(4);
		Object[]            objs = new Object[7];
		Arrays.fill(objs, "");

		LongStream.rangeClosed(0, count)
		          .mapToObj(date::plusDays)
		          .forEachOrdered(d -> {
			          if (func.apply(d)) {
				          Employee empl = content.getByDate(d);
				          objs[dayNumFunc.apply(d) - 1] = d.getDayOfMonth() + "\n" + empl.toString();
			          }
			          if (dayNumFunc.apply(d) % 7 == 0) {
				          list.add(objs.clone());
				          Arrays.fill(objs, "");
			          }
		          });

		return new ListOfArrayDataSource(list, WEEK_DAYS_NAME);
	}

	public void view() {
		JasperViewer.viewReport(print);
	}

	public void exportToFile() {
		try {
			String destFileName = QuarterReport.class.getResource("/jrxml_reports/quarter_report/")
			                                         .getPath() + "1.pdf";
			JasperExportManager.exportReportToPdfFile(print, destFileName);
			LOG.trace("file pdf " + destFileName);
		} catch (JRException e) {
			LOG.error("Error when export to pdf", e);
		}
	}

	private String cyr_MonthName_TxtRepr(Month ISO_month) {
		switch (ISO_month.getValue()) {
			case 1:
				return "Січень";
			case 2:
				return "Лютий";
			case 3:
				return "Березень";
			case 4:
				return "Квітень";
			case 5:
				return "Травень";
			case 6:
				return "Червень";
			case 7:
				return "Липень";
			case 8:
				return "Серпень";
			case 9:
				return "Вересень";
			case 10:
				return "Жовтень";
			case 11:
				return "Листопад";
			case 12:
				return "Грудень";
			default:
				return "";
		}
	}
}