package com;

import com.krm.reporting.QuarterReport;

import net.sf.jasperreports.engine.JRException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDate;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import javafx.application.Application;
import javafx.concurrent.Task;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Main extends Application {
	private static final Logger LOG = LogManager.getLogger(Main.class);
	private AtomicReference<LocalDate> refDate = new AtomicReference<>(LocalDate.now().plusMonths(4));

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) {
		Task bgWork = new Task() {
			@Override
			protected Void call() {
				LOG.debug("call from TASK");
				LocalDate toDate = LocalDate.of(refDate.get().getYear(), refDate.get().getMonth(), 1);
				LOG.debug(toDate);
				QuarterReport report = new QuarterReport(toDate);
				report.build();
				report.view();
				return null;
			}
		};

		BlockingQueue<Runnable> queue = new LinkedBlockingQueue<>(5);
		final ExecutorService ex = new ThreadPoolExecutor(2, 2, 10, TimeUnit.MICROSECONDS, queue);

		stage.setTitle("Check date");

		AnchorPane ap = new AnchorPane();
		ap.autosize();

		VBox vb = new VBox();
		vb.setSpacing(2d);
		vb.setPadding(new Insets(5));
		vb.setAlignment(Pos.CENTER);

		Label tf = new Label("To:");

		DatePicker datePicker = new DatePicker(refDate.get());
		datePicker.setPrefSize(120, 25);

		Button btn = new Button("Ok");
		btn.setPrefSize(120, 45);
		btn.setCancelButton(true);
		btn.setOnAction(e -> {
			refDate.getAndSet(datePicker.getValue());
			ex.submit(bgWork);
		});

		vb.getChildren().addAll(tf, datePicker, btn);
		ap.getChildren().add(vb);

		stage.setScene(new Scene(ap));
		stage.sizeToScene();
		stage.setResizable(false);
		stage.show();
	}
}
