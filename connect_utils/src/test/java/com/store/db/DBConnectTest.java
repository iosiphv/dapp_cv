package com.store.db;

import com.store.ContentResolver;
import com.store.Employee;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.*;

/**
 * Created by ovt10 on 11.11.2016.
 */
public class DBConnectTest {
	private final static Logger LOG = LogManager.getLogger(DBConnectTest.class.getName());
	private ContentResolver<Employee> dbConn;

	@Before
	public void setUp() throws Exception {
		dbConn = DBConnect.getInstance();
	}

	@Test
	public void getInstance() throws Exception {
		assertNotNull("connect cannot be null",DBConnect.getInstance());
	}

	@Test
	public void insert() throws Exception {

	}

	@Test
	public void getByEmployee() throws Exception {

	}

	@Test
	public void getByDate() throws Exception {
		Employee empl = dbConn.getByDate(LocalDate.now());
		LOG.debug(empl.name());
		assertNotNull("employee must be not null", empl);
		assertNotNull("employee name must be not null", empl.name());
	}

	@Test
	public void getMaxDate() throws Exception {

	}

	@Test
	public void close() throws Exception {

	}

}