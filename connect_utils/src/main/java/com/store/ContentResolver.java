package com.store;

import java.time.LocalDate;
import java.util.TreeSet;

/**
 * Created by SeJay on 19.02.2015.
 */
public interface ContentResolver<T> {
	int insert(T empl, LocalDate date);

	TreeSet<LocalDate> getByEmployee(T e);

	T getByDate(LocalDate searchdate);

	LocalDate getMaxDate();

	void close();
}
