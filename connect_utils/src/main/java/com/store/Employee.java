package com.store;

public enum Employee {
	EMPTY(0, ""),
	USER1(1, "USER1"),
	USER2(2, "USER2"),
	USER3(3, "USER3"),
	USER4(4, "USER4"),
	USER5(5, "USER5"),
	USER6(6, "USER6"),
	USER7(7, "USER7");

	private String surname;
	private int id;

	Employee(int id, String surname) {
		this.surname = surname;
		this.id = id;
	}

	@Override
	public String toString() {
		return surname;
	}

	public int toID() {
		return id;
	}

	public static Employee get(int i) {
		switch (i) {
			case 1:
				return USER1;
			case 2:
				return USER2;
			case 3:
				return USER3;
			case 4:
				return USER4;
			case 5:
				return USER5;
			case 6:
				return USER6;
			case 7:
				return USER7;
			default:
				return EMPTY;
		}
	}

}
