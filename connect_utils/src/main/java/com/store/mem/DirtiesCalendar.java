package com.store.mem;

import com.store.ContentResolver;
import com.store.Employee;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class DirtiesCalendar implements ContentResolver<Employee> {
	private static final Logger LOG = LogManager.getLogger(DirtiesCalendar.class);

	private final HashMap<Employee, TreeSet<LocalDate>> calendar = new HashMap<>(8);

	private boolean isGetted = false;

	public DirtiesCalendar() {
		calendar.put(Employee.USER6, new TreeSet<>());
		calendar.put(Employee.USER2, new TreeSet<>());
		calendar.put(Employee.USER3, new TreeSet<>());
		calendar.put(Employee.USER5, new TreeSet<>());
		calendar.put(Employee.USER1, new TreeSet<>());
	}

	@Override
	public int insert(Employee empl, LocalDate date) {
		if (empl.equals(Employee.EMPTY))
			throw new IllegalArgumentException("Must be not \"EMPTY\"");
		return calendar.get(empl).add(date) ? 1 : 0;
	}

	@Override
	public TreeSet<LocalDate> getByEmployee(Employee e) {
		return calendar.get(e);
	}

	@Override
	public Employee getByDate(LocalDate searchdate) {
		return calendar.keySet().parallelStream()
		               .filter(e -> calendar.get(e).contains(searchdate))
		               .findFirst().orElse(Employee.EMPTY);
	}

	@Override
	public LocalDate getMaxDate() {
		Comparator<LocalDate> comp = (a, b) -> a.isBefore(b) ? 1 : 0;
		return
				calendar
						.values().parallelStream()
						.flatMap(Collection::stream)
						.collect(Collectors.toSet()).parallelStream()
						.max(comp)
						.orElse(LocalDate.of(1970, 1, 1));
	}

	@Override
	public void close() {
		calendar.clear();
	}
}