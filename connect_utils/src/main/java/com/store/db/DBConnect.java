package com.store.db;

import com.store.ContentResolver;
import com.store.Employee;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.TreeSet;

public class DBConnect implements ContentResolver<Employee> {
	private final static Logger LOG = LogManager.getLogger(DBConnect.class.getName());

	//	private final static String DB_PATH = DBConnect.class.getResource("/data_base").getPath();
  private final static String DB_PATH = "D:/workspace/java/Dirting_App_CV/data_base";

	private final static String DB_NAME = "DB";

	private final static String INSERT_ONE_SQL       = "INSERT INTO DIRTY_CALENDAR(ID_DIRTY, Dirting_DATE) VALUES (?,?)";
	private final static String SELECT_EMPL_BY_DATE  = "SELECT d.ID_DIRTY FROM DIRTY_CALENDAR d " +
			"WHERE d.Dirting_DATE = ?";
	private final static String SELECT_ARRAY_BY_EMPL = "SELECT d.Dirting_DATE FROM DIRTY_CALENDAR d " +
			"WHERE d.ID_DIRTY = ?";
	private final static String SELECT_BY_EMPL_DATE  = "SELECT COUNT(*) FROM DIRTY_CALENDAR d " +
			"WHERE d.ID_DIRTY = ? AND d.Dirting_DATE = ?";

	private static Connection connection = null;
	private static DBConnect  instance   = null;

	private DBConnect() {}

	private static Connection connect() {
		String path = DB_PATH + "/" + DB_NAME;
		String url  = "jdbc:h2:file:" + path + ";DB_CLOSE_DELAY=1;ifexists=FALSE";
		String i = "jdbc:h2:file:D:/WorkSpace/Java/dirting_app_cv/connect_utils/src/main/resources/data_base/DB";
		try {
			Class.forName("org.h2.Driver");
			connection = DriverManager.getConnection(i+ ";DB_CLOSE_DELAY=1;ifexists=FALSE", "SA", "");
			LOG.trace("Connect success to " + DB_PATH + "/" + DB_NAME + "\t" + LocalDateTime.now());
		} catch (SQLException | ClassNotFoundException e) {
			LOG.fatal("cannot connect {}\nurl:{}", e.getLocalizedMessage(), url);
			throw new RuntimeException(e.getCause());
		}
		return connection;
	}

	public static DBConnect getInstance() {
		synchronized (DBConnect.class) {
			if (instance == null) instance = new DBConnect();
			if (connection == null) connect();
		}
		return instance;
	}

	public int insert(Employee empl, LocalDate date) {
		int  count     = -1;
		long checkLong = 0;

		LOG.debug("try to insert empl {} date {}", empl, date);

		Date sqlDate = Date.valueOf(date);

		try (
				PreparedStatement ps = connection.prepareStatement(SELECT_EMPL_BY_DATE)
		) {
			ps.setDate(1, sqlDate);
			ps.execute();
			ResultSet res = ps.executeQuery();
			if (res.next()) checkLong = res.getLong(1);
			res.close();
		} catch (SQLException e) {
			LOG.error("{} {} empl {} date {}", e.getErrorCode(), e.getMessage(), empl, date);
		}

		if (checkLong == 0)
			try (
					PreparedStatement ps = connection.prepareStatement(INSERT_ONE_SQL)
			) {
				ps.setInt(1, empl.toID());
				ps.setDate(2, sqlDate);
				count = ps.executeUpdate();
				LOG.debug("insert empl {} date {}", empl, date);
			} catch (SQLException e) {
				LOG.error("{} empl {} date {}", e.getMessage(), empl, date);
				count = -1;
			}
		return count;
	}

	public TreeSet<LocalDate> getByEmployee(Employee e) {
		TreeSet<LocalDate> result = new TreeSet<>();
		try (PreparedStatement statement = connection.prepareStatement(SELECT_ARRAY_BY_EMPL)) {
			statement.setInt(1, e.toID());
			ResultSet res = statement.executeQuery();

			while (res.next()) result.add(res.getDate(1).toLocalDate());

			res.close();
		} catch (SQLException e1) {
			LOG.error(e1.getMessage());
		}
		return result;
	}

	public Employee getByDate(LocalDate searchdate) {
		Employee result;
		try (PreparedStatement statement = connection.prepareStatement(SELECT_EMPL_BY_DATE)) {
			statement.setDate(1, Date.valueOf(searchdate));
			ResultSet res = statement.executeQuery();
			res.next();
			result = Employee.get(res.getInt("ID_DIRTY"));
			res.close();
		} catch (SQLException e) {
			result = Employee.EMPTY;
			LOG.error(e.getMessage());
		}
		return result;
	}

	public LocalDate getMaxDate() {
		LocalDate date = LocalDate.MIN;
		try (
				ResultSet res = connection
						.createStatement()
						.executeQuery("SELECT  MAX(d.DIRTING_DATE) FROM  DIRTY_CALENDAR d")
		) {
			res.next();
			date = res.wasNull() ? LocalDate.MIN : res.getDate(1).toLocalDate();
		} catch (SQLException e) {
			LOG.error(e.getMessage());
			LOG.error("DB " + DB_PATH + "/" + DB_NAME + "\t" + LocalDateTime.now());
		}
		return date;
	}

	public void truncDB() {
		try {
			connection.createStatement().executeUpdate("DELETE from DIRTY_CALENDAR");
		} catch (SQLException e) {
			LOG.fatal(e.getMessage());
		}
	}

	public void close() {
		try {
			synchronized (this) {
				if (connection != null && !connection.isClosed()) {
					connection.close();
					LOG.trace("Close success from " + DB_PATH + "/" + DB_NAME + "\t" + LocalDateTime.now());
				}
			}
		} catch (SQLException e) {
			LOG.fatal(e.getMessage());
		}
	}
}
